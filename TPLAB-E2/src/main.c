/*
 * This file is part of the ÂµOS++ distribution.
 *   (https://github.com/micro-os-plus)
 * Copyright (c) 2014 Liviu Ionescu.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use,
 * copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom
 * the Software is furnished to do so, subject to the following
 * conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 * OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

// ----------------------------------------------------------------------------

#include <stdio.h>
#include <stdlib.h>
#include "diag/Trace.h"
#include "stm32f4xx.h"

// ----------------------------------------------------------------------------
//
// STM32F4 empty sample (trace via STDOUT).
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the STDOUT output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace_impl.c
// (currently OS_USE_TRACE_ITM, OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).
//

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"


void GPIO_Configure_Button(void);
void GPIO_Configure_leds(void);
void SysTick_Handler(void);

int main(int argc, char* argv[])
{
  // At this stage the system clock should have already been configured
  // at high speed.

	HAL_Init ();
	GPIO_Configure_Button();
	GPIO_Configure_leds();
	SysTick_Handler();
	GPIO_PinState estado; //Define una estructura de enumeración que contenga estados booleanos


  // Infinite loop
  while (1)
    {

	  	  // Controlar parpadeo LED RED

		  HAL_Delay(300);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_SET);
		  HAL_Delay(300);
		  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_14, GPIO_PIN_RESET);


		  // Pulsador selecciona LED - VERDE

		  estado = HAL_GPIO_ReadPin(GPIOA, GPIO_PIN_0);
		  if(estado == GPIO_PIN_SET){
		     HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_SET); //LED VERDE - ENCIENDE
		  }else{
			  HAL_GPIO_WritePin(GPIOD, GPIO_PIN_12, GPIO_PIN_RESET); //LED VERDE - APAGA
		  }


    }
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------



void GPIO_Configure_leds(void){
	 __HAL_RCC_GPIOD_CLK_ENABLE(); //Habilita el clock para GPIOD

	GPIO_InitTypeDef GPIO_leds;
	GPIO_leds.Pin = GPIO_PIN_12|GPIO_PIN_14;
	GPIO_leds.Mode = GPIO_MODE_OUTPUT_PP; // establece el modo del pin en salida

	HAL_GPIO_Init(GPIOD, &GPIO_leds); // inicializa los pins PD12-PD14 pasando el nombre del puerto y la dir de GPIO_leds
}


void GPIO_Configure_Button(void){
	__HAL_RCC_GPIOA_CLK_ENABLE(); //Habilita el clock para GPIOA

	GPIO_InitTypeDef GPIO_Button;
	GPIO_Button.Mode = GPIO_MODE_INPUT; //establece el modo del pin como entrada
	GPIO_Button.Pin = GPIO_PIN_0;
	GPIO_Button.Pull = GPIO_NOPULL;

	HAL_GPIO_Init(GPIOA, &GPIO_Button); // inicializa el pin PA0 pasando el nombre del puerto y la dir de GPIO_Button
}

void SysTick_Handler(void){
  HAL_IncTick();
}
